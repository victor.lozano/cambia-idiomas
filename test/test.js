const expect = require('chai').expect
const nuevo_idioma = require('..').default

describe('#nuevo_idioma', () => {
  it('Si la palabra termina en "ar", se le quitan esas dos letras', () => {
    const traduccion = nuevo_idioma("Programar")
    expect(traduccion).to.equal("Program")
  })
  it('Si la palabra inicia con Z, se le añade "pe" al final', () => {
    const traduccionZorro = nuevo_idioma("Zorro")
    expect(traduccionZorro).to.equal("Zorrope")
    const traduccionZarpar = nuevo_idioma("Zarpar")
    expect(traduccionZarpar).to.equal("Zarppe")
  })
  it('Si la palabra traducida tiene 10 o más letras, se debe partir en dos por la mitad y unir con un guión medio', () => {
    const traduccion = nuevo_idioma("abecedario")
    expect(traduccion).to.equal("abece-dario")
  })
  it('Si la palabra original es un palíndromo, ninguna regla anterior cuenta y se devuelve la misma palabra pero intercalando letras mayúsculas y minúsculas', () => {
    const traduccion = nuevo_idioma("sometemos")
    expect(traduccion).to.equal("SoMeTeMoS")
  })
})
