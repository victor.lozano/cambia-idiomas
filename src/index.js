export default function nuevo_idioma(cadena) {
  const invertir = (cadena) => cadena.split('').reverse().join('')

  const intercalada = (cadena) => {
    const longitud = cadena.length
    let traduccion = ''
    let mayuscula = true
    for(let i = 0; i < longitud; i++) {
      traduccion += mayuscula ? cadena.charAt(i).toUpperCase() : cadena.charAt(i).toLowerCase()
      mayuscula = !mayuscula
    }
    return traduccion
  }

  // Si la palabra original es un palíndromo, ninguna regla anterior cuenta y se devuelve la misma palabra pero intercalando letras mayúsculas y minúsculas
  if(cadena == invertir(cadena)) return intercalada(cadena)

  let traduccion = cadena

  // Si la palabra termina en "ar", se le quitan esas dos letras
  if(cadena.toUpperCase().endsWith('AR')) traduccion = cadena.slice(0, -2)

  // Si la palabra inicia con Z, se le añade "pe" al final
  if(cadena.toUpperCase().startsWith('Z')) traduccion += 'pe'

  // Si la palabra traducida tiene 10 o más letras, se debe partir en dos por la mitad y unir con un guión medio
  const longitud = traduccion.length
  if(longitud >= 10) {
    const primerMitad = traduccion.slice(0, Math.round(longitud / 2))
    const segundaMitad = traduccion.slice(Math.round(longitud / 2), longitud)
    traduccion = `${primerMitad}-${segundaMitad}`
  }

  return traduccion
}
