[![pipeline status](https://gitlab.com/bardux/cambia-idiomas/badges/master/pipeline.svg)](https://gitlab.com/bardux/cambia-idiomas/commits/master) [![coverage report](https://gitlab.com/bardux/cambia-idiomas/badges/master/coverage.svg)](https://gitlab.com/bardux/cambia-idiomas/commits/master)

# Nuevo idioma

Este modulo es un idioma inventado para npm

## Descripción del idioma

- Si la palabra termina en "ar", se le quitan esas dos letras
- Si la palabra inicia con Z, se le añade "pe" al final
- Si la palabra traducida tiene 10 o más letras, se debe partir en dos por la mitad y unir con un guión medio
- Si la palabra original es un palíndromo, ninguna regla anterior cuenta y se devuelve la misma palabra pero intercalando letras mayúsculas y minúsculas

## Instalación
```
npm install nuevo_idioma
```

## Uso
```
import nuevo_idioma from 'index'

nuevo_idioma("Programar") // Program
nuevo_idioma("Zorro") // Zorrope
nuevo_idioma("Zarpar") // Zarppe
nuevo_idioma("abecedario") // abece-dario
nuevo_idioma("sometemos") // SoMeTeMoS
```

## Créditos
- [Victor Lozano](https://twitter.com/imbardux)

## Licencia
[MIT](https://opensource.org/licenses/MIT)
